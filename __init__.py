from st3m.application import Application
from st3m.input import InputController
import leds


RECORDING_TIME = 5000


class TapBPM(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

        self.input = InputController()
        self._last_taps = []
        self._generated_taps = []
        self._previously_pressed = False
        self._current_beat_delay = -1
        self._intra_beat_state = 0  # n from 0 to 1e6

        self._is_new_beat = False
        self._beat_hsv_state = 0

        leds.set_slew_rate(255)


    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        # Last beat history
        ctx.move_to(-100, 20).rgb(31, 142, 221).line_to(100, 20).stroke()
        for tap in self._last_taps:
            pos = (tap * 200) // 5000
            ctx.move_to(100 - pos, 15).rgb(93, 181, 244).line_to(100 - pos, 20).stroke()
        for tap in self._generated_taps:
            pos = (tap * 200) // 5000
            ctx.move_to(100 - pos, 21).rgb(93, 181, 244).line_to(100 - pos, 25).stroke()

        # BPM
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 42
        if self._current_beat_delay > 0:
            ctx.move_to(0, -30)
            ctx.rgb(0, 255, 255).text(str(60e3 // self._current_beat_delay)[:-2] + " bpm")

        # LEDs
        if self._is_new_beat:
            self._generated_taps.append(0)
            leds.set_all_hsv(self._beat_hsv_state, 1.0, 1.0)

            self._beat_hsv_state = (self._beat_hsv_state + 111) % 360
            self._is_new_beat = False

        leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)
        petal = self.input.captouch.petals[5]
        pressed = petal.pressure > 0

        # Update taps
        for tap_list in (self._last_taps, self._generated_taps):
            i = 0
            while i < len(tap_list):
                tap_list[i] += delta_ms
                if tap_list[i] > RECORDING_TIME:
                    tap_list.pop(i)
                else:
                    i += 1

        # Intra Beat Time
        if self._current_beat_delay > 0:
            self._intra_beat_state += (1e6 * delta_ms) // self._current_beat_delay
            if self._intra_beat_state > 1e6:
                self._intra_beat_state %= 1e6
                self._is_new_beat = True

        # Beat updates
        if pressed and not self._previously_pressed:
            self._last_taps.append(0)
            if len(self._last_taps) > 3:
                deltas_sum = 0
                for i in range(len(self._last_taps) - 1):
                    deltas_sum += self._last_taps[i] - self._last_taps[i + 1]
                self._current_beat_delay = deltas_sum / (len(self._last_taps) - 1)
                self._intra_beat_state = 0
                self._is_new_beat = True

        self._previously_pressed = pressed


if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(TapBPM(ApplicationContext()))
